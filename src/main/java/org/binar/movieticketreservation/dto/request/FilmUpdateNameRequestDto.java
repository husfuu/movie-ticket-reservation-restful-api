package org.binar.movieticketreservation.dto.request;

import lombok.Data;

@Data
public class FilmUpdateNameRequestDto {
    private String filmName;
}
